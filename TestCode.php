<?php

    class JokesController
    {
        public function __construct(){
            $this->filter('auth');
        }

        /**
         * Display a listing.
         */
        public function getList(Request $request){
            $search_term = $request->input('search');
            $limit = $request->input('limit',5);
            if ($search_term)
            {
                $jokes = Joke::orderBy('id', 'DESC')->where('body', 'LIKE', "%$search_term%")->with(
                array('User'=>function($query){
                    $query->select('id','name');
                })
                )->select('id', 'body', 'user_id')->paginate($limit); 
     
                $jokes->appends(array(
                    'search' => $search_term,
                    'limit' => $limit
                ));
            }
            else
            {
                $jokes = Joke::orderBy('id', 'DESC')->with(
                array('User'=>function($query){
                    $query->select('id','name');
                })
                )->select('id', 'body', 'user_id')->paginate($limit); 
     
                $jokes->appends(array(            
                    'limit' => $limit
                ));
            }
            return response()->json($this->transformCollection($jokes));
        }

        /**
         * Store a newly created resource in storage.
         */
        public function insert(Request $request)
        {
     
            if(! $request->body or ! $request->user_id){
                return response()->json([
                    'error' => [
                        'message' => 'Please Provide Both body and user_id'
                    ]
                ]);
            }
            $joke = Joke::create($request->all());
            return response()->json([
                    'message' => 'Joke Created Succesfully',
                    'data' => $this->transform($joke)
            ]);
        }

        /**
         * Display the specified resource.
         */
        public function show($id){
                $joke = Joke::with(
                array('User'=>function($query){
                    $query->select('id','name');
                })
                )->find($id);
         
                if(!$joke){
                    return response()->json(['error'=>['message' => 'Joke does not exist']]);
                }
                // get previous joke id
                $previous = Joke::where('id', '<', $joke->id)->max('id');
         
                // get next joke id
                $next = Joke::where('id', '>', $joke->id)->min('id');
                return response()->json([
                    'previous_joke_id'=> $previous,
                    'next_joke_id'=> $next,
                    'data' => $this->transform($joke)
                ]);
        }


        /**
         * Update the specified resource in storage.
         */
        public function update(Request $request, $id)
        {    
            if(! $request->body or ! $request->user_id){
                return response()->json([
                    'error' => [
                        'message' => 'Please Provide Both body and user_id'
                    ]
                ]);
            }
            
            $joke = Joke::find($id);
            $joke->body = $request->body;
            $joke->user_id = $request->user_id;
            $joke->save(); 
     
            return response()->json([
                    'message' => 'Joke Updated Succesfully'
            ]);
        }

        /**
         * Remove the specified resource from storage.
         */
        public function destroy($id)
        {
            Joke::destroy($id);
        }

        private function transformCollection($jokes){
            $jokesArray = $jokes->toArray();
            return [
                'total' => $jokesArray['total'],
                'per_page' => intval($jokesArray['per_page']),
                'current_page' => $jokesArray['current_page'],
                'last_page' => $jokesArray['last_page'],
                'next_page_url' => $jokesArray['next_page_url'],
                'prev_page_url' => $jokesArray['prev_page_url'],
                'from' => $jokesArray['from'],
                'to' =>$jokesArray['to'],
                'data' => array_map([$this, 'transform'], $jokesArray['data'])
            ];
        }


        private function transform($joke){
            return [
                   'joke_id' => $joke['id'],
                   'joke' => $joke['body'],
                   'submitted_by' => $joke['user']['name']
                ];
        }

    }

?>